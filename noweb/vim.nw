% Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

\section{{\noweb} et {\vim}}
\label{sec:vim}

\paragraph{} Un fichier {\noweb} n'étant rien de plus qu'un fichier texte
mélangeant {\LaTeX} et code, n'importe quel éditeur peut être utilisé pour
rédiger un {\noweb}. Se pose cependant la question du support de la coloration
syntaxique dans l'éditeur choisi, ce dernier devant identifier les parties
{\LaTeX} des parties de codes et utiliser la bonne coloration syntaxique dans
chacune des parties.

\paragraph{} Pour l'éditeur de texte {\vim} cette fonctionnalité peut être
ajoutée simplement en utilisant le fichier de syntaxe \texttt{noweb.vim}
disponible sur le site officiel de {\vim}
(\url{https://www.vim.org/scripts/script.php?script_id=3038}). Pour ce faire,
il faut d'abord copier ce fichier dans le répertoire \texttt{\$HOME/.vim/syntax}.

<<install the noweb.vim syntax script>>=
mkdir -p $HOME/.vim/syntax/
wget https://www.vim.org/scripts/download_script.php?src_id=13268 -O $HOME/.vim/syntax/noweb.vim
@

\paragraph{} Nous pouvons alors modifier le fichier \texttt{.vimrc} pour
configurer l'utilisation de ce script. Trois variables contrôlent son
comportement~:

\begin{itemize}
  \item [[noweb_language]]~: détermine le langage qui est utilisé dans les
  blocs de code~;
  \item [[noweb_backend]]~: désigne le langage utilisé dans le texte~;
  \item [[noweb_fold_code]]~: contrôle si les blocs de code doivent être
  automatiquement ``repliés".
\end{itemize}

\paragraph{} Nous pouvons par exemple ajouter les lignes suivantes au fichier
\texttt{.vimrc}~:

<<\$HOME/.vimrc>>=
let noweb_language="c"
let noweb_backend="tex"
let noweb_fold_code=0
@

\paragraph{} Reste alors à configurer {\vim} de telle sorte que les fichiers
avec une extension donnée (par exemple \texttt{*.nw}) soient reconnus comme des
fichiers {\noweb} devant automatiquement utiliser la coloration syntaxique
définie dans le fichier \texttt{noweb.vim}. Pour cela, et conformément à la
documentation de {\vim} (\url{https://vimhelp.org/vim_faq.txt.html#faq-26.8}),
nous définissons un fichier \texttt{filetype.vim} comme suit:

<<\$HOME/.vim/filetype.vim>>=
 " my filetype file
if exists("did_load_filetypes")
    finish
endif
augroup filetypedetect
    au! BufRead,BufNewFile *.nw setfiletype noweb
augroup END
@

\paragraph{} Ne reste plus alors qu'à s'assurer que la coloration syntaxique
est activée dans {\vim}

<<\$HOME/.vimrc>>=
syntax on
@


