% Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

\chapter{Licences}
\label{sec:licenses}

\paragraph{} Dans cette annexe nous listons les en-têtes de fichier qui
notifient les détenteurs des droits patrimoniaux sur le code ainsi que la
licence qui le régit. Une même licence est parfois présente plusieurs fois, la
seule différence étant les caractères de commentaire utilisés pour insérer cet
en-tête dans les différents fichiers. 

\paragraph{} Seules deux licences différentes sont utilisées dans les codes
source que nous développons ici. La quasi totalité des codes sources, y compris
les sources du présent document, sont licenciés sous la \textit{GNU General
Public License} version 3~\cite{GPLv3} dont l'en-tête {\C} est le suivant.

<<licensing>>=
/* Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */
@

\paragraph{} Une copie de cette licence est distribuée avec les sources de ce
programme. Vous pouvez également la consulter à l'url
\url{https://www.gnu.org/licenses/gpl-3.0.html}. En plus de l'en-tête pour le
langage {\C}, nous listons ci-après ce même en-tête qui utilise le caractère
`\#' comme caractère de commentaire afin de pouvoir l'insérer dans un fichier
de configuration {\cmake} ou dans un script \texttt{Bash}.

<<licensing formatted with \# comments>>=
# Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
@

\paragraph{} Enfin nous utilisons également la \textit{GNU All-Permissive
licence} qui, comme son nom l'indique, est particulièrement laxiste. Nous
appliquons cette licence uniquement sur des petits scripts \texttt{Bash}.

<<GNU All-Permissive license>>=
# Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
@

