% Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

\chapter{\'Ecrire un programme lettré avec {\noweb}}
\label{chap:noweb}

\paragraph{} Ce chapitre propose une suite d'outils pour rédiger des programmes
lettrés dont le présent document. Cet environnement de développement se base
sur {\noweb}, un outil de programmation lettrée indépendant des langages de
programmation, et qui utilise {\LaTeX} comme langage de description.

\paragraph{} Plusieurs raisons nous poussent à utiliser {\noweb}. Tout d'abord
{\LaTeX} est un puissant langage de description, installé depuis longtemps dans
une large communauté très active.  De plus, de par son indépendance aux
langages de programmation, {\noweb} permet de rédiger des codes dans différents
langages, et ce dans un seul et même document~; cet écrit contient par exemple
des scripts \texttt{GNU Bash} et des codes sources en {\C}. Au delà de ses seules
fonctionnalités affichées, {\noweb} s'inscrit avant tout dans une pensée UNIX~:
il se présente comme un ensemble d'outils, combinés les uns aux autres à l'aide
de \textit{pipe} UNIX et de fichiers textes. Chaque outil reste donc simple,
s'intéresse à une seule tâche et est facilement extensible comme l'illustre la
section~\ref{sec:cross-ref}. {\noweb} s'insère dès lors parfaitement dans un
écosystème UNIX dont il adopte les principes. Et il devient alors tout aussi
naturel d'utiliser {\noweb} comme un nouvel outil à côté de \texttt{awk} ou
\texttt{gcc}, que de le piloter à l'aide d'un système de génération automatique
de fichiers tel que {\GNUmake}.

\paragraph{} {\noweb} présente cependant plusieurs limites et en premier lieu
desquelles celle de ne plus être en développement actif. Le problème est ici
plus large que {\noweb} et touche plus généralement l'ensemble des outils de
programmation lettrée. Ce paradigme reste aujourd'hui marginal dans la
communauté informatique. Par conséquent, peu d'outils de programmation lettrée
sont aujourd'hui encore développés, maintenus ou utilisés. Dans ce contexte,
{\noweb} reste néanmoins un des outils les plus aboutis et un des plus
éprouvés.

\paragraph{} Dans la section~\ref{sec:install} nous commençons par décrire
comment installer {\noweb} sur un système GNU/Linux. Dans le
section~\ref{sec:vim} nous proposons d'utiliser l'éditeur de texte {\vim} pour
rédiger un programme lettré. Pour ce faire, nous décrivons comment modifier et
configurer {\vim} pour rendre plus ergonomique l'édition de fichiers {\noweb} à
travers notamment le support de la coloration syntaxique.
% TODO Ajouter dans cette section une partie sur l'utilisation de "ctag"
La section~\ref{sec:cross-ref} décrit comment modifier le comportement
par défaut de {\noweb} pour structurer notre document à l'aide des directives
{\LaTeX} \texttt{{\textbackslash}input} et \texttt{{\textbackslash}include},
tout en conservant le support du référencement croisé des blocs de codes
apporté par {\noweb}. Enfin la section~\ref{sec:build_pdf} présente le système
de génération automatique utilisé pour générer le présent document.

\input{install}
\input{vim}
\input{weave}
\input{build_pdf}
