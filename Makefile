# Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

SHELL:=/bin/bash

include config.mk

DOC = litpt

NOWEB = \
	noweb/intro.nw \
	noweb/conclusion.nw \
	noweb/noweb.nw \
	noweb/build_pdf.nw \
	noweb/path_tracer.nw \
	noweb/install.nw \
	noweb/vim.nw \
	noweb/weave.nw \
	noweb/pt_app.nw \
	noweb/pt_args.nw \
	noweb/pt_build.nw \
	noweb/pt_camera.nw \
	noweb/pt_image.nw \
	noweb/pt_main.nw \
	noweb/pt_make.nw \
	noweb/pt_run.nw \
	noweb/license.nw \
	noweb/$(DOC).nw
SRC = \
	$(BUILD_DIR)/split_scope.bash \
	cmake/CMakeLists.txt \
	src/pt.h \
	src/pt.c \
	src/pt_args.h \
	src/pt_args.c \
	src/pt_camera.h \
	src/pt_camera.c \
	src/pt_image.h \
	src/pt_image.c \
	src/pt_main.c
IMG = \
	etc/sponza.png \
	etc/camera.pdf \
	etc/brdf.pdf \
	etc/pixel.pdf

PDFLATEX_OPTS += -output-directory=$(BUILD_DIR)
DIRS = $(BUILD_DIR) cmake src tex

NOPATH=$(shell dirname `which noweave`)
PDFLATEX=TEXINPUTS+=:$(shell pwd)/tex pdflatex $(PDFLATEX_OPTS)
BIBTEX=BIBINPUTS+=./:$(shell pwd)/noweb bibtex

TEX = $(NOWEB:noweb/%.nw=tex/%.tex)

.PHONY: all pdf clean bin
all: $(SRC) $(TEX)

pdf: $(BUILD_DIR)/$(DOC).pdf

clean:
	rm -rf $(SRC) $(TEX); \
	ls $(BUILD_DIR) | grep -v $(DOC).pdf | xargs -I {} rm -rf $(BUILD_DIR)/{}

bin: $(BUILD_DIR)/Makefile etc/sponza.obj
	make -C $(BUILD_DIR)

$(BUILD_DIR)/Makefile: $(SRC) config.mk
	cd $(BUILD_DIR) && cmake ../cmake \
		-DCMAKE_PREFIX_PATH=$(STAR_ENGINE_PATH) \
		-DCMAKE_BUILD_TYPE=$(BUILD_TYPE)

$(BUILD_DIR)/$(DOC).pdf: $(TEX) $(BUILD_DIR)/$(DOC).blg $(IMG)
	$(PDFLATEX) $(DOC) && $(PDFLATEX) $(DOC)

$(BUILD_DIR)/$(DOC).blg: noweb/biblio.bib
	$(PDFLATEX) $(DOC) && cd $(BUILD_DIR) && $(BIBTEX) $(DOC)

$(BUILD_DIR)/weave.all: $(NOWEB) $(BUILD_DIR)/split_scope.bash config.mk
	noweave $(NOWEAVE_OPTS) -backend "bash $(BUILD_DIR)/split_scope.bash " $(NOWEB)

$(TEX): tex/%.tex: noweb/%.nw $(BUILD_DIR)/weave.all
$(DIRS):
	mkdir -p $@

$(SRC): $(NOWEB) $(DIRS) config.mk
	@echo Tangle $@
	@notangle $(NOTANGLE_OPTS) -R$(shell echo $@ | sed 's/_/\\\\_/') $(NOWEB) | cpif $@

etc/sponza.obj: etc/sponza.obj.xz
	xz -dfk etc/sponza.obj.xz
