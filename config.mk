# Copyright (C) 2019 |Meso|Star> (vincent.forest@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

STAR_ENGINE_PATH=$(HOME)/Star-Engine-0.8.0-GNU-Linux64
BUILD_TYPE=Release
BUILD_DIR=build

NOTANGLE_OPTS = #-L
NOWEAVE_OPTS = -index -delay -v
