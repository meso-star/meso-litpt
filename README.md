# Meso Literate Path-Tracer

Ce programme s'appuie sur le
[Star-Engine](https://gitlab.com/meso-star/star-engine) pour mettre en oeuvre
un moteur de rendu minimaliste rédigé suivant le paradigme de la [programmation
lettrée](http://www.literateprogramming.com/). Dans ce paradigme, le
développement d'une application s'assimile à la rédaction d'un document. Ce
faisant, en même temps qu'ils rédigent un texte, les auteurs développent un
programme dont la structure suit leurs seuls choix d'écriture. Avant d'être un
moteur de rendu, le présent projet est donc avant tout un ouvrage qui décrit
comment utiliser le `Star-Engine` pour développer une application de calcul
scientifique par Monte-Carlo interagissant avec des géométries 3D. Il présente
également la suite d'outils et l'environnement de travail utilisés pour sa
rédaction et tend à porter une réflexion plus générale sur la signification de
la programmation lettrée dans un acte de développement informatique.

## Générer le document

Au delà du seul code source, ce programme décrit également l'ensemble de la
procédure permettant de générer le document associé, compiler l'exécutable ou
encore lancer le rendu d'une image. Ceci étant dit, pour pouvoir lire ce
document, faut il encore pouvoir le générer. Ce programme s'appuie sur l'outil
de programmation lettrée [noweb](https://www.ctan.org/pkg/noweb) et par
conséquent utilise LaTeX comme langage de description. Par ailleurs, nous
utilisons GNU Make pour gérer la génération automatique du document associé à
notre programme. Une fois ces pré-requis installés, d'aucun peut alors générer
ledit document en invoquant simplement la commande suivante :

    make pdf

A l'issue du cette commande, le pdf du programme peut dès lors être consulté
directement à l'aide d'un simple lecteur pdf :

    mupdf build/litpt.pdf

## Droits patrimoniaux & licence

Copyright (C) 2019 [|Meso|Star>](https://www.meso-star.com)
(vincent.forest@meso-star.com). Ce programme est un logiciel libre distribué
sous la licence GPLv3+. Vous pouvez le redistribuer sous certaines conditions ;
prière de se référer au fichier COPYING pour connaître l'ensemble des détails.
